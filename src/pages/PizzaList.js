import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

export default class PizzaList extends Component {
	constructor(data) {
		super(
			'section',
			{ name: 'class', value: 'pizzaList' },
			data.map(function (pizza) {
				return new PizzaThumbnail(pizza);
			})
		);
	}
}
