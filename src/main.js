import data from './data.js';
import Component from './components/Component.js';
import Img from './components/Img.js';
import PizzaThumbnail from './components/PizzaThumbnail.js';
import PizzaList from './pages/PizzaList.js';
import Router from './Router.js';

const title = new Component('h1', null, ['La', ' ', 'carte']);
document.querySelector('.pageTitle').innerHTML = title.render();

/*const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);
document.querySelector('.pageContent').innerHTML = c.render();*/

/*const pizza = data[0];
const pizzaThumbnail = new PizzaThumbnail(pizza);
document.querySelector('.pageContent').innerHTML = pizzaThumbnail.render();*/

const pizzaList = new PizzaList(data);
document.querySelector('.pageContent').innerHTML = pizzaList.render();

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');
Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];
console.log(document.querySelector('.pageTitle'));
