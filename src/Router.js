import PizzaList from './pages/PizzaList';

class Router {
	static titleElement;
	static contentElement;
	static routes;

	static navigate(path) {
		for (let i = 0; i < routes.length; i++) {
			if (routes[i].path == path) {
				Router.titleElement.innerHTML = new Component(
					'h1',
					null,
					routes[i].title
				);
				Router.contentElement.innerHTML = new PizzaList();
			}
		}
	}
}
