export default class Component {
	tagName;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}
	render() {
		if (this.children == null) {
			return `<${this.tagName} ${this.renderAttribute()}/>`;
		}
		if (this.attribute == null) {
			return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`;
		}
		return `<${
			this.tagName
		} ${this.renderAttribute()}>${this.renderChildren()}</${this.tagName}>`;
	}
	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
	}
	renderChildren() {
		if (this.children instanceof Array) {
			let result = ``;
			for (let i = 0; i < this.children.length; i++) {
				if (this.children[i] instanceof Component) {
					result += this.children[i].render();
				} else {
					result += this.children[i];
				}
			}
			return result;
		}
		return `${this.children}`;
	}
}
